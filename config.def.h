/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;
static const unsigned int snap      = 32;
static const unsigned int systraypinning = 0;
static const unsigned int systrayonleft = 0;
static const unsigned int systrayspacing = 2;
static const int systraypinningfailfirst = 1;
static const int showsystray        = 1;
static const int showbar            = 1;
static const int topbar             = 1;
static const Bool viewontag         = True;
/* static const char *fonts[]          = { "WenQuanYi Micro Hei Mono:size=10", "JoyPixels:size=10" }; */
static const char *fonts[]          = { "mononoki Nerd Font:size=10:style=Bold", "WenQuanYi Micro Hei Mono:size=11", "JoyPixels:size=10" };
static const char dmenufont[]       = "WenQuanYi Micro Hei Mono:size=10";
static const char col_gray1[]       = "#000000";
static const char col_gray2[]       = "#333333";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#ffffff";
static const char col_cyan[]        = "#778979";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray1 },
	[SchemeSel]  = { col_gray3, col_gray2, col_cyan  },
};

/* tagging */
/* static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" }; */
static const char *tags[] = { "일", "이", "삼", "사", "오", "육", "칠", "팔", "구" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
/* class          	 instance    title    tags mask    switchtotag    isfloating    monitor */
	{ "qutebrowser",     NULL,     NULL,       1 << 1,       1,             0,           -1 },
	{ "Firefox",         NULL,     NULL,       1 << 1,       1,             0,           -1 },
	{ "Thunar",          NULL,     NULL,       1 << 2,       1,             0,           -1 },
	{ "mpv",             NULL,     NULL,       1 << 4,       1,             1,           -1 },
	{ "qBittorrent",     NULL,     NULL,       1 << 5,       1,             0,           -1 },
	{ "Zathura",         NULL,     NULL,       1 << 7,       1,             0,           -1 },
	
	{ NULL,              NULL,    "newsboat",  1 << 0,       1,             0,           -1 },
	{ NULL,              NULL,    "ranger",    1 << 2,       1,             0,           -1 },
	{ NULL,              NULL,    "cmus",      1 << 3,       1,             0,           -1 },
	
	{ "Sxiv",            NULL,     NULL,            0,       0,             1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ Mod4Mask,                     XK_Return, spawn,          {.v = termcmd } },
	{ Mod1Mask|ControlMask,         XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },

	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)

	/* I don't use the default keybinding above */
	{ Mod1Mask,                     XK_0,      view,           {.ui = ~0 } }, /* view all tags */
	{ Mod1Mask|ShiftMask,           XK_0,      tag,            {.ui = ~0 } }, /* send window to all tags */
	{ Mod1Mask|ControlMask,         XK_Left,   shiftview,      { .i = -1 } }, /* navigate to the left active tag */
	{ Mod1Mask|ControlMask,         XK_Right,  shiftview,      { .i = +1 } }, /* navigate to the right active tag */
	
	{ Mod4Mask,                     XK_Left,   focusstack,     {.i = -1 } }, /* focus on the left window */
	{ Mod4Mask,                     XK_Right,  focusstack,     {.i = +1 } }, /* focus on the right window */

	{ Mod1Mask,                     XK_Left,   setmfact,       {.f = -0.01} }, /* adjust wondwo size horizontally */
	{ Mod1Mask,                     XK_Right,  setmfact,       {.f = +0.01} }, /* adjust wondwo size horizontally */

	{ Mod1Mask,                     XK_i,      incnmaster,     {.i = +1 } },
	{ Mod1Mask,                     XK_d,      incnmaster,     {.i = -1 } },
	{ Mod1Mask,                     XK_Return, zoom,           {0} }, /* promote focused window to master */
	{ Mod1Mask,                     XK_Tab,    view,           {0} },
	
	
	{ Mod1Mask,                     XK_space,  setlayout,      {0} },
	{ Mod4Mask|ShiftMask,           XK_space,  togglefloating, {0} }, /* toggle floating */
	{ Mod4Mask,                     XK_s,      togglesticky,   {0} }, /* toggle sticky */
	{ Mod4Mask|ShiftMask,           XK_q,      killclient,     {0} }, /* kill a window */
	{ Mod4Mask|ShiftMask,      	    XK_c,      quit,           {0} }, /* quit dwm */

};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
